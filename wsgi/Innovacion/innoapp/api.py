from django.contrib.auth.models import User
from innoapp.models import Event, Participant
from rest_framework import routers, serializers, viewsets

# Serializers define the API representation.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email')

class EventSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Event 
        fields = ('name', 'prizes', 'genre', 'rules', 'pdf_url', 'contact_detail', 'register_url','icon',
        'banner_img')

class ParticipantSerializer(serializers.HyperlinkedModelSerializer):
    user = UserSerializer(read_only =True)
    class Meta:
        model = Participant
        fields = ('name', 'part_id','user' ,'contact' ,'dept' ,'year' ,'inst')

# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    
class EventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer    
    
class ParticipantViewSet(viewsets.ModelViewSet):
    serializer_class = ParticipantSerializer
    def get_queryset(self):
        queryset = Participant.objects.all()
        part_id = self.request.query_params.get('part_id',None)
        if part_id is not None:
            queryset = queryset.filter(part_id=part_id)
        return queryset
        

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'events', EventViewSet)
router.register(r'participants', ParticipantViewSet,'participants')