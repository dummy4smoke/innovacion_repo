from django.db import models
from django.contrib.auth.models import User,Group
# Create your models here.
#
'''
class User(models.Model):
    namename = models.CharField(max_length=20)
    email = models.CharField(max_length=20,primary_key=True)
    password = models.CharField(max_length=20)
    
class Admin(models.Model):
    name = models.CharField(max_length=20)
    user = models.OneToOneField(User, primary_key=True)
'''

class Participant(models.Model):
    name = models.CharField(max_length=20)
    part_id = models.CharField(max_length=20)
    user = models.OneToOneField(User)
    contact = models.CharField(max_length=20)
    dept = models.CharField(max_length=20)
    year = models.PositiveSmallIntegerField()
    inst = models.CharField(max_length=30)

class Event(models.Model):
    name = models.CharField(max_length=20)
    prizes = models.CharField(max_length=40)
    genre = models.CharField(max_length=10)
    rules = models.TextField()
    icon = models.URLField(max_length=200)
    banner_img = models.URLField(max_length=200)
    pdf_url = models.FileField(max_length=200)
    contact_detail = models.CharField(max_length=40)
    register_url = models.URLField(max_length=200)
    
class Managers(models.Model):
    name = models.CharField(max_length=20)
    event = models.ForeignKey(Event)
    

class Team(models.Model):
    name = models.CharField(max_length=20)
    event = models.ForeignKey(Event)
    participants = models.ManyToManyField(Participant)

class Registration(models.Model):
    participant = models.ForeignKey(Participant)
    time_stamp = models.DateField()
    

class EventRegistraion(models.Model):
    participant = models.OneToOneField(Participant)
    event_name = models.OneToOneField(Event)
    